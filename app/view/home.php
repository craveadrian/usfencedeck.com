<div id="services">
	<div class="row">
		<div class="service1">
			<div class="image">
				<img src="public/images/content/service1.jpg" alt="Service 1">
			</div>
			<div class="text right">
				<div class="container">
					<h3>METAL, VINYL <span>& WOOD FENCE</span></h3>
					<p>Do you have plans to fence your property in Hendersonville, TN? Whether you are planning on fencing a business or a residential property, we can take care of the job for you expertly at US Fence & Deck. We are highly skilled and licensed craftsmen with over 30 years of experience providing high-quality work in the homes and businesses in the area.</p>
					<a href="#" class="btn">LEARN MORE</a>
				</div>
			</div>
		</div>
		<div class="service2">
			<div class="text left">
				<div class="container">
					<h3>CHAIN LINK FENCE <span>& DECK REPAIR</span></h3>
					<p>Are you a home or business owner in the greater Nashville, TN area that needs a quality chain link fence installed on your property? If so, there is a great local company that can take care of the job for you impeccably! We are US Fence & Deck, and we are experts in quality fence installations of all types including Chain Link. </p>
					<a href="#" class="btn">LEARN MORE</a>
				</div>
			</div>
			<div class="image">
				<img src="public/images/content/service2.jpg" alt="Service 2">
			</div>
		</div>
		<div class="service3">
			<div class="image">
				<img src="public/images/content/service3.jpg" alt="Service 3">
			</div>
			<div class="text right">
				<div class="container">
					<h3>DECK <span>INSTALLATION </span></h3>
					<p>Are you ready to start creating the backyard living space of your dreams in your home in Gallatin, TN?  If so, call us at US Fence & Deck. No other contractor in the area offers the skill, experience, and great value that we do. We are responsible for some of the most beautiful decks that you can see in the area. </p>
					<a href="#" class="btn">LEARN MORE</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="welcome">
	<div class="row">
		<div class="top">
			<div class="text">
				<div class="container">
					<h4>WELCOME TO</h4>
					<h1>US FENCE <span>& DECK</span></h1>
					<p>If you are in the greater Madison, TN area and are interested in having fence installation work done, there is only one contractor that you’ll ever need to call. We are US Fence & Deck, and we are the area’s finest fencing contractor. Our prices are competitive, and we work with a variety of materials expertly. So whether you are looking for vinyl, wood or metal fence installation, we are the experts who can take care of the job for you. We’ve been doing this since 1984 and have over 30 years of experience doing impeccable work for the area’s home and business owners. We, therefore, know a thing or two about fencing! We are committed to delivering the finest work and pride ourselves on our quality of work and attention to detail. Our company is licensed as a member of the local area Chamber of Commerce. We set the standard in fencing, decks, and other backyard enhancements in the community. No matter how large or small the job you have is, you are always assured of our trademark quality and professionalism. We are local and nearby to cities in the Madison, TN area including Nashville, Henderson, and Portland. Give us a call for a quote today! </p>
					<div class="buttons">
						<a href="#" class="btn">LEARN MORE</a>
						<div class="call">
							<h5>CALL US TODAY!</h5>
							<?php $this->info(["phone","tel","welcome-phone"]); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="main-images">
				<img src="public/images/content/wlc-img.jpg" alt="Welcome Image">
			</div>
		</div>
		<div class="bottom">
			<div class="container-images">
				<img src="public/images/content/wlc-img1.jpg" alt="Welcome Image 1">
				<img src="public/images/content/wlc-img2.jpg" alt="Welcome Image 2">
				<img src="public/images/content/wlc-img3.jpg" alt="Welcome Image 3">
				<img src="public/images/content/wlc-img4.jpg" alt="Welcome Image 4">
			</div>
		</div>
	</div>
</div>
<div id="contact">
	<div class="row">
		<div class="top">
			<h2>US FENCE & DECK WORK WITH A VARIETY OF QUALITY FENCING MATERIALS</h2>
			<p>Whether you are looking for a wood or metal fence installation, we can do the job impeccably. When it comes to the finest fence work in the area, we simply can’t be beaten. For the best fence installation job, make sure you choose our company! </p>
			<div class="call">
				<h5>CALL US TODAY </h5>
				<?php $this->info(["phone","tel","contact-phone"]); ?>
			</div>
		</div>
		<div class="bottom">
			<div class="contact-form">
				<h1>Contact Us</h1>
				<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
					<label><span class="ctc-hide">Name</span>
						<input type="text" name="name" placeholder="Name:">
					</label>
					<label><span class="ctc-hide">Email</span>
						<input type="text" name="email" placeholder="Email:">
					</label>
					<label><span class="ctc-hide">Phone</span>
						<input type="text" name="phone" placeholder="Phone:">
					</label>
					<label><span class="ctc-hide">Message</span>
						<textarea name="message" cols="30" rows="10" placeholder="Message:"></textarea>
					</label>
					<label for="g-recaptcha-response"><span class="ctc-hide">Recaptcha</span></label>
					<div class="g-recaptcha"></div>
					<label>
						<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
					</label><br>
					<?php if( $this->siteInfo['policy_link'] ): ?>
					<label>
						<input type="checkbox" name="termsConditions" class="termsBox"/> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
					</label>
					<?php endif ?>
					<button type="submit" class="ctcBtn" disabled>Submit</button>
				</form>
			</div>
			<div class="map">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d102955.40388078598!2d-86.76814851236357!3d36.255126084755894!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x886442569b04100b%3A0x26cec139d12008cd!2sMadison%2C+Nashville%2C+TN+37115%2C+USA!5e0!3m2!1sen!2sph!4v1563274029891!5m2!1sen!2sph" allowfullscreen></iframe>
			</div>
		</div>
	</div>
</div>
